#include "ChNil.h"

#include <SoftwareSerial.h>

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define RX             11
#define TX             10
#define RTS_LED        12
#define RS485Transmit  HIGH
#define RS485Receive   LOW
SoftwareSerial RS485Serial(RX, TX);

#define SCREEN_WIDTH   128
#define SCREEN_HEIGHT  32
#define SCREEN_ADDRESS 0x3C
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

#define BTUP   4
#define BTDOWN 7
#define BTS    8

#define IN1    3
#define IN2    5
#define IN3    6
#define IN4    9

#define ENCODER 2

byte RS485_received[8];

int16_t slaveAddress;
int16_t functionCode;
int16_t reg;
int16_t totalReg;

char buffPWM[20];
char buffRPM[20];
char buffKec[20];

int PWM;
int countValue = 0;
int state = 0;

volatile int pulse = 0;
double rps = 0;
double rpm = 0;
int rpmMod = 0;

unsigned long timelast;

int redPin = A0;
int greenPin = A1;
int bluePin = A2;

THD_WORKING_AREA(waMain, 64);
THD_WORKING_AREA(waTombol, 64);

THD_FUNCTION(Main, arg) {
  (void)arg;

  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  bool level = true;

  pinMode(ENCODER, INPUT);

  attachInterrupt(digitalPinToInterrupt(ENCODER), encoder, CHANGE);

  while (TRUE) {
    digitalWrite(LED_BUILTIN, level = !level);
    chThdSleep(20);

    //OLED & MOTOR
    PWM = map(countValue, 0, 100, 0, 255);

    if (millis() - timelast >= 100) {
      detachInterrupt(digitalPinToInterrupt(ENCODER));
      timelast = millis();
      rps = (pulse / 1.0) / 0.1;
      rpm = rps * 60;

      pulse = 0;

      attachInterrupt(digitalPinToInterrupt(ENCODER), encoder, CHANGE);
    }

    display.clearDisplay();

    display.setRotation(2);
    display.setCursor(23, 0);
    display.println("PWM Controller");

    sprintf(buffPWM, "PWM    : %d <-> %d%%\n", PWM, countValue);
    display.setCursor(0, 10);
    display.println(buffPWM);

    dtostrf(rpm, 4, 2, buffRPM);
    sprintf(buffKec, "RPM : %s", buffRPM);
    display.setCursor(0, 20);
    display.println(buffKec);

    if (state == 0) {
      //      display.setCursor(0, 20);
      //      display.println("Status : Off");

      analogWrite(IN1, 0);
      analogWrite(IN2, 0);
    }
    else if (state == 1) {
      //      display.setCursor(0, 20);
      //      display.println("Status : On");

      analogWrite(IN1, PWM);
      analogWrite(IN2, 0);
    }

    display.display();
  }
}

THD_FUNCTION(Tombol, arg) {
  (void)arg;

  pinMode(BTUP, INPUT);
  pinMode(BTDOWN, INPUT);
  pinMode(BTS, INPUT);

  while (TRUE) {
    if (digitalRead(BTUP) == HIGH) {
      countValue++;
      if (countValue > 100) {
        countValue = 100;
      }
    }
    else if (digitalRead(BTDOWN) == HIGH) {
      countValue--;
      if (countValue < 0) {
        countValue = 0;
      }
    }
    else if (digitalRead(BTS) == HIGH) {
      if (state == 0) {
        state = 1;
      }
      else if (state == 1) {
        state = 0;
      }
    }
    chThdSleep(100);
  }
}

THD_TABLE_BEGIN
THD_TABLE_ENTRY(waMain, "main", Main, NULL)
THD_TABLE_ENTRY(waTombol, "tombol", Tombol, NULL)
THD_TABLE_END

void setup() {
  chBegin();

  RS485Serial.begin(9600);

  Serial.begin(115200);

  pinMode(RTS_LED, OUTPUT);

  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);

  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }
  delay(2000);

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
}

void loop() {
  digitalWrite(RTS_LED, RS485Receive);
  RS485Serial.readBytes(RS485_received, sizeof(RS485_received));

  slaveAddress = RS485_received[0];
  functionCode = RS485_received[1];
  reg = RS485_received[2] << 8 | RS485_received[3];
  totalReg = RS485_received[4] << 8 | RS485_received[5];

  if (slaveAddress == 1) {

    //Function Code = 1
    if (functionCode == 1) {
      Serial.println("Function Code = 1");
    }

    //Function Code = 3
    if (functionCode == 3) {
      byte dataTransmit[11];

      if (totalReg == 1) {
        if (reg == 3678) {
          dataTransmit[0] = slaveAddress;
          dataTransmit[1] = functionCode;
          dataTransmit[2] = totalReg * 2;
          dataTransmit[3] = 0;
          dataTransmit[4] = countValue;

          unsigned int crcHi = ModRTU_CRC(sizeof(dataTransmit) - 6, dataTransmit) >> 8;
          unsigned int crcLo = ModRTU_CRC(sizeof(dataTransmit) - 6, dataTransmit);

          dataTransmit[5] = crcLo;
          dataTransmit[6] = crcHi;

          //Mengirim ke Modbus
          digitalWrite(RTS_LED, RS485Transmit);
          RS485Serial.write(dataTransmit, sizeof(dataTransmit));
          RS485Serial.flush();
        }
        if (reg == 3679) {
          dataTransmit[0] = slaveAddress;
          dataTransmit[1] = functionCode;
          dataTransmit[2] = totalReg * 2;
          dataTransmit[3] = 0;
          rpmMod = rpm;
          dataTransmit[4] = rpmMod;
          Serial.println(rpmMod);

          unsigned int crcHi = ModRTU_CRC(sizeof(dataTransmit) - 6, dataTransmit) >> 8;
          unsigned int crcLo = ModRTU_CRC(sizeof(dataTransmit) - 6, dataTransmit);

          dataTransmit[5] = crcLo;
          dataTransmit[6] = crcHi;

          //Mengirim ke Modbus
          digitalWrite(RTS_LED, RS485Transmit);
          RS485Serial.write(dataTransmit, sizeof(dataTransmit));
          RS485Serial.flush();
        }
        if (reg == 3680) {
          dataTransmit[0] = slaveAddress;
          dataTransmit[1] = functionCode;
          dataTransmit[2] = totalReg * 2;
          dataTransmit[3] = 0;
          dataTransmit[4] = state;

          unsigned int crcHi = ModRTU_CRC(sizeof(dataTransmit) - 6, dataTransmit) >> 8;
          unsigned int crcLo = ModRTU_CRC(sizeof(dataTransmit) - 6, dataTransmit);

          dataTransmit[5] = crcLo;
          dataTransmit[6] = crcHi;

          //Mengirim ke Modbus
          digitalWrite(RTS_LED, RS485Transmit);
          RS485Serial.write(dataTransmit, sizeof(dataTransmit));
          RS485Serial.flush();
        }
      }

      if (totalReg == 2) {
        if (reg == 3678) {
          dataTransmit[0] = slaveAddress;
          dataTransmit[1] = functionCode;
          dataTransmit[2] = totalReg * 2;
          dataTransmit[3] = 0;
          dataTransmit[4] = countValue;
          dataTransmit[5] = 0;
          dataTransmit[6] = rps;

          unsigned int crcHi = ModRTU_CRC(sizeof(dataTransmit) - 4, dataTransmit) >> 8;
          unsigned int crcLo = ModRTU_CRC(sizeof(dataTransmit) - 4, dataTransmit);

          dataTransmit[7] = crcLo;
          dataTransmit[8] = crcHi;

          //Mengirim ke Modbus
          digitalWrite(RTS_LED, RS485Transmit);
          RS485Serial.write(dataTransmit, sizeof(dataTransmit));
          RS485Serial.flush();
        }
        if (reg == 3679) {
          dataTransmit[0] = slaveAddress;
          dataTransmit[1] = functionCode;
          dataTransmit[2] = totalReg * 2;
          dataTransmit[3] = 0;
          dataTransmit[4] = rps;
          dataTransmit[5] = 0;
          dataTransmit[6] = state;

          unsigned int crcHi = ModRTU_CRC(sizeof(dataTransmit) - 4, dataTransmit) >> 8;
          unsigned int crcLo = ModRTU_CRC(sizeof(dataTransmit) - 4, dataTransmit);

          dataTransmit[7] = crcLo;
          dataTransmit[8] = crcHi;

          //Mengirim ke Modbus
          digitalWrite(RTS_LED, RS485Transmit);
          RS485Serial.write(dataTransmit, sizeof(dataTransmit));
          RS485Serial.flush();
        }
        if (reg == 3680) {
          dataTransmit[0] = slaveAddress;
          dataTransmit[1] = 83;
          dataTransmit[2] = 2;

          unsigned int crcHi = ModRTU_CRC(sizeof(dataTransmit) - 8, dataTransmit) >> 8;
          unsigned int crcLo = ModRTU_CRC(sizeof(dataTransmit) - 8, dataTransmit);

          dataTransmit[3] = crcLo;
          dataTransmit[4] = crcHi;

          //Mengirim ke Modbus
          digitalWrite(RTS_LED, RS485Transmit);
          RS485Serial.write(dataTransmit, sizeof(dataTransmit));
          RS485Serial.flush();
        }
      }

      if (totalReg == 3) {
        if (reg == 3678) {
          dataTransmit[0] = slaveAddress;
          dataTransmit[1] = functionCode;
          dataTransmit[2] = totalReg * 2;
          dataTransmit[3] = 0;
          dataTransmit[4] = countValue;
          dataTransmit[5] = 0;
          dataTransmit[6] = rps;
          dataTransmit[7] = 0;
          dataTransmit[8] = state;

          unsigned int crcHi = ModRTU_CRC(sizeof(dataTransmit) - 2, dataTransmit) >> 8;
          unsigned int crcLo = ModRTU_CRC(sizeof(dataTransmit) - 2, dataTransmit);

          dataTransmit[9] = crcLo;
          dataTransmit[10] = crcHi;

          //Mengirim ke Modbus
          digitalWrite(RTS_LED, RS485Transmit);
          RS485Serial.write(dataTransmit, sizeof(dataTransmit));
          RS485Serial.flush();
        }
        if (reg == 3679) {
          dataTransmit[0] = slaveAddress;
          dataTransmit[1] = 83;
          dataTransmit[2] = 2;

          unsigned int crcHi = ModRTU_CRC(sizeof(dataTransmit) - 8, dataTransmit) >> 8;
          unsigned int crcLo = ModRTU_CRC(sizeof(dataTransmit) - 8, dataTransmit);

          dataTransmit[3] = crcLo;
          dataTransmit[4] = crcHi;

          //Mengirim ke Modbus
          digitalWrite(RTS_LED, RS485Transmit);
          RS485Serial.write(dataTransmit, sizeof(dataTransmit));
          RS485Serial.flush();
        }
        if (reg == 3680) {
          dataTransmit[0] = slaveAddress;
          dataTransmit[1] = 83;
          dataTransmit[2] = 2;

          unsigned int crcHi = ModRTU_CRC(sizeof(dataTransmit) - 8, dataTransmit) >> 8;
          unsigned int crcLo = ModRTU_CRC(sizeof(dataTransmit) - 8, dataTransmit);

          dataTransmit[3] = crcLo;
          dataTransmit[4] = crcHi;

          //Mengirim ke Modbus
          digitalWrite(RTS_LED, RS485Transmit);
          RS485Serial.write(dataTransmit, sizeof(dataTransmit));
          RS485Serial.flush();
        }
      }
    }

    //Function Code = 5
    if (functionCode == 5) {
      Serial.println("Function Code = 5");
    }

    //Function Code = 6
    if (functionCode == 6) {
      if (reg == 3678) {
        //Mengirim ke Modbus
        digitalWrite(RTS_LED, RS485Transmit);
        RS485Serial.write(RS485_received, sizeof(RS485_received));
        RS485Serial.flush();

        countValue = totalReg;
      }
      if (reg == 3679) {
        //Mengirim ke Modbus
        digitalWrite(RTS_LED, RS485Transmit);
        RS485Serial.write(RS485_received, sizeof(RS485_received));
        RS485Serial.flush();

        rpm = totalReg;
      }
      if (reg == 3680) {
        //Mengirim ke Modbus
        digitalWrite(RTS_LED, RS485Transmit);
        RS485Serial.write(RS485_received, sizeof(RS485_received));
        RS485Serial.flush();

        state = totalReg;
      }
    }
    memset(RS485_received, 0, sizeof(RS485_received));
  }

  //red
  rgb (1, 0, 0);
  delay(100);

  //green
  rgb (0, 1, 0);
  delay(100);

  //blue
  rgb (0, 0, 1);
  delay(100);
}

void encoder() {
  pulse++;
}

unsigned int ModRTU_CRC(int len, unsigned char *buf) {
  unsigned int crc = 0xFFFF;
  int pos;
  int i;

  for (pos = 0; pos < len; pos++) {
    crc ^= (unsigned int)buf[pos]; // XOR byte into least sig. byte of crc

    for (i = 8; i != 0; i--) { // Loop over each bit
      if ((crc & 0x0001) != 0) { // If the LSB is set
        crc >>= 1; // Shift right and XOR 0xA001
        crc ^= 0xA001;
      }
      else // Else LSB is not set
        crc >>= 1; // Just shift right
    }
  }
  // Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
  return crc;
}

void rgb (int red, int green, int blue) {
  digitalWrite(redPin, red);
  digitalWrite(greenPin, green);
  digitalWrite(bluePin, blue);
}
